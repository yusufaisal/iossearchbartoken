//
//  ViewController.swift
//  SearchBar
//
//  Created by iSal on 06/12/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var textfield: UITextField!
    var cntr = 0
    var items:[String] = [] {
        didSet {
            searchBar.searchTextField.tokens.removeAll()
            for (i,item) in self.items.enumerated() {
                let token = UISearchToken(icon: nil, text: item)
                searchBar.searchTextField.insertToken(token, at: i)
                
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textfield.delegate = self
        self.searchBar.delegate = self
        searchBar.searchTextField.allowsCopyingTokens = true
        searchBar.searchTextField.allowsDeletingTokens = false
        searchBar.searchTextField.tokenBackgroundColor = .systemPink
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.items.removeAll()
        searchBar.searchTextField.tokens.removeAll()
    }



    @IBAction func addItem(_ sender: Any) {
        self.items.append(String(cntr))
        self.cntr += 1
    }
    
}

extension ViewController: UISearchBarDelegate{
    
}
